#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from std_srvs.srv import Trigger, TriggerResponse
import time
from hotrider_lib.srv import SetRoute
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.robot_status = "pause"
        self.game_message = "initializing"

        self.point_a = [-2.5, -3.5]
        self.point_b = [-3.5, 0.5]
        self.point_c = [0.5, 2.5]
        self.point_d = [4.5, 1.5]

        self.departure_point = "A"
        self.destination_point = "B"

        self.metrics = {
            'robot_status' : self.robot_status,
            'robot_name' : self.robot_name,
            'departure_point' : self.departure_point,
            'destination_point' : self.destination_point,
            'game_message' : self.game_message
        }


        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)
        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        rospy.Subscriber("robot_pose", String, self.pose_callback)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        while ~rospy.is_shutdown():
            self.publish_metrics()
            rate.sleep()
            if self.robot_status == "stop":
                break


    def handle_robot(self,req):
        return TriggerResponse(
            success = True,
            message = self.robot_status
        )

    def clear_heats(self):
        try:
            rospy.wait_for_service("/clear_heats", 5.0)
            clear_heats = rospy.ServiceProxy('clear_heats', Trigger)
            resp = clear_heats()
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def set_destination(self, x, y):
        try:
            rospy.wait_for_service("/set_destination", 5.0)


            time.sleep(0.1)
            set_route = rospy.ServiceProxy('/set_destination', SetRoute)
            resp = set_route(x, y)

            if not resp.status:
                self.robot_status = "stop"
        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))

    def publish_metrics(self):
        time.sleep(0.1)
        self.metrics['robot_status'] = str(self.robot_status)
        self.metrics['destination_point'] = self.destination_point
        self.metrics['departure_point'] = self.departure_point
        self.metrics['game_message'] = self.game_message
        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def pose_callback(self, data):
        data_string = data.data.split(":")
        pose_data = [float(data_string[1]), float(data_string[2])]
        if (pose_data[0] == self.point_a[0]) and (pose_data[1] == self.point_a[1]):
            self.robot_status = "pause"
            self.clear_heats()
            self.set_destination(self.point_b[0], self.point_b[1])
            self.robot_status = "run"
            self.game_message = "running"
            self.departure_point = "A"
            self.destination_point = "B"
        elif (pose_data[0] == self.point_b[0]) and (pose_data[1] == self.point_b[1]):
            self.robot_status = "pause"
            self.clear_heats()
            self.set_destination(self.point_c[0], self.point_c[1])
            self.robot_status = "run"
            self.departure_point = "B"
            self.destination_point = "C"
        elif (pose_data[0] == self.point_c[0]) and (pose_data[1] == self.point_c[1]):
            self.robot_status = "pause"
            self.clear_heats()
            self.set_destination(self.point_d[0], self.point_d[1])
            self.robot_status = "run"
            self.departure_point = "C"
            self.destination_point = "D"
        elif (pose_data[0] == self.point_d[0]) and (pose_data[1] == self.point_d[1]):
            print("D")
            self.robot_status = "stop"
            self.game_message = "finished course"
    
    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

if __name__ == '__main__':
    # Name of robot
    controller = GameController("hotrider")
